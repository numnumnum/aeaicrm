package com.agileai.crm.module.mytasks.handler;

import com.agileai.crm.cxmodule.ColdCallsManage;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.MasterSubEditPboxHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.util.StringUtil;

public class ProCustVisitInfoHandler
        extends MasterSubEditPboxHandler {
    public ProCustVisitInfoHandler() {
        super();
        this.serviceId = buildServiceId(ColdCallsManage.class);
        this.subTableId = "ProCustVisitInfo";
    }

    protected void processPageAttributes(DataParam param) {
        if (!StringUtil.isNullOrEmpty(param.get("TASK_ID"))) {
            this.setAttribute("TASK_ID", param.get("TASK_ID"));
        }
        setAttribute("PROCUST_VISIT_EFFECT",
                FormSelectFactory.create("VISIT_EFFECT")
                                 .addSelectedValue(getOperaAttributeValue("PROCUST_VISIT_EFFECT",
                                                                          "")));
	    setAttribute("PROCUST_VISIT_TYPE",
	           FormSelectFactory.create("VISIT_TYPE")
	                            .addSelectedValue(getOperaAttributeValue("PROCUST_VISIT_TYPE",
	                                                                     "")));
    }

    protected ColdCallsManage getService() {
        return (ColdCallsManage) this.lookupService(this.getServiceId());
    }
}
