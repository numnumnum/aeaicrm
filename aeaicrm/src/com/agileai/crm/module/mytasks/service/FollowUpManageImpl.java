package com.agileai.crm.module.mytasks.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.FollowUpManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubServiceImpl;
import com.agileai.util.DateUtil;

public class FollowUpManageImpl
        extends MasterSubServiceImpl
        implements FollowUpManage {
    public FollowUpManageImpl() {
        super();
    }

    public String[] getTableIds() {
        List<String> temp = new ArrayList<String>();

        temp.add("_base");
        temp.add("MyCustVisitInfo");

        return temp.toArray(new String[] {  });
    }

	@Override
	public DataRow getMyCustRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getMyCustRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public void createCustVisitRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertCustVisitRecord";
		param.put("VISIT_ID", KeyGenerator.instance().genKey());
		
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public void updateTaskStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateTaskStateRecord";
		this.daoHelper.updateRecord(statementId, param);
	}
	
	@Override
	public DataRow getCustInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustInfoRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public void updateCustStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateCustStateRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public void updateTaskClassRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateTaskClassRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public void createCustRecord(DataParam param) {
		DataParam custParam = new DataParam();
		String uuidCust = param.get("custUuid");
		custParam.put("CUST_ID", uuidCust);
		custParam.put("CUST_NAME", param.get("ORG_NAME"));
		custParam.put("CUST_PROGRESS_STATE", "PRELIMINARY");
		custParam.put("CUST_LEVEL", "ORDINARY");
		String dateStr = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, new Date());
		custParam.put("CUST_CREATE_TIME", dateStr);
		custParam.put("CUST_CONFIRM_TIME", dateStr);
		custParam.put("CUST_CREATE_ID", param.get("VISIT_USER_ID"));
		custParam.put("CUST_STATE", "init");
		custParam.put("ORG_ID", param.get("ORG_ID"));
		String statementId = sqlNameSpace+"."+"insertCustomerInfoRecord";
		this.daoHelper.insertRecord(statementId, custParam);
		
		statementId = sqlNameSpace+"."+"getGroupRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, new DataParam("GRP_CODE", "TEMP"));
		String grpId = dataRow.getString("GRP_ID");
		
		statementId = sqlNameSpace+"."+"insertCustomerInfoRelation";
		this.daoHelper.insertRecord(statementId, new DataParam("CUST_ID",uuidCust,"GRP_ID",grpId));
		
		statementId = sqlNameSpace+"."+"addUserTreeRelation";
		this.daoHelper.insertRecord(statementId, new DataParam("CUST_ID",uuidCust,"USER_ID",param.get("VISIT_USER_ID")));
		
		statementId = sqlNameSpace+"."+"updateCustIdRecord";
		this.daoHelper.insertRecord(statementId, new DataParam("CUST_ID",uuidCust,"ORG_ID",param.get("ORG_ID")));
		
		statementId = sqlNameSpace+"."+"updateProCustIdRecord";
		this.daoHelper.insertRecord(statementId, new DataParam("CUST_ID",uuidCust,"ORG_ID",param.get("ORG_ID")));
	}

	@Override
	public DataRow getTaskReviewRecord(DataParam param) {
		String statementId = "TaskCycle8ContentManage"+"."+"getTaskReviewRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public DataRow getCustVisitInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustVisitInfoRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public DataRow getCustPhoneInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustPhoneInfoRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}
}
