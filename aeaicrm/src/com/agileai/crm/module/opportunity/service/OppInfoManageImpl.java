package com.agileai.crm.module.opportunity.service;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.OppInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class OppInfoManageImpl
        extends StandardServiceImpl
        implements OppInfoManage {
    public OppInfoManageImpl() {
        super();
    }

	public void createOrderRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"createOrderRecord";
		String clueId = KeyGenerator.instance().genKey();
   		param.put("ORDER_ID",clueId);
		processDataType(param, tableName);
		this.daoHelper.insertRecord(statementId, param);
		}

	@Override
	public void changeStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"changeStateRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public void changeStateRecord(String oppId) {
		DataParam param = new DataParam("OPP_ID", oppId,"OPP_STATE","2");
		String statementId = sqlNameSpace+"."+"changeStateRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
		
	}

	@Override
	public void createContRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"createContRecord";
		String contId = KeyGenerator.instance().genKey();
   		param.put("CONT_ID",contId);
		processDataType(param, tableName);
		processPrimaryKeys(param);
		this.daoHelper.insertRecord(statementId, param);
		
	}

	@Override
	public DataRow getCustStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustStateRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}		
}
