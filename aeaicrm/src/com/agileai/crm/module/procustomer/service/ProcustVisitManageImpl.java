package com.agileai.crm.module.procustomer.service;

import com.agileai.crm.cxmodule.ProcustVisitManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class ProcustVisitManageImpl
        extends StandardServiceImpl
        implements ProcustVisitManage {
    public ProcustVisitManageImpl() {
        super();
    }

	@Override
	public DataRow getCustRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
}
