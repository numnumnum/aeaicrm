<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>潜在客户拜访记录</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function closeBox(){
	parent.closeBox();

}
function goToBackList(){
	parent.goToBack();
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>
<%if(pageBean.getBoolValue("isClose")){%>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="closeBox()"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>   
<%} %>
<%if(pageBean.getBoolValue("goBack")){%>  
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBackList();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
<%} %>   
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="潜在客户拜访记录.csv"
retrieveRowsCallback="process" xlsFileName="潜在客户拜访记录.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="315px"
>
<ec:row styleClass="odd" ondblclick="doRequest('viewDetail')" oncontextmenu="selectRow(this,{PROCUST_VISIT_ID:'${row.PROCUST_VISIT_ID}',CUST_VISIT_CATEGORY:'${row.CUST_VISIT_CATEGORY}'});refreshConextmenu()" onclick="selectRow(this,{PROCUST_VISIT_ID:'${row.PROCUST_VISIT_ID}',CUST_VISIT_CATEGORY:'${row.CUST_VISIT_CATEGORY}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="PROCUST_VISIT_DATE" title="拜访日期" cell="date" format="yyyy-MM-dd" />
	<ec:column width="100" property="PROCUST_VISIT_FILL_NAME" title="填写人" />
	<ec:column width="100" property="PROCUST_VISIT_FILL_TIME" title="填写时间" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="PROCUST_VISIT_EFFECT" title="拜访效果"   mappingItem="PROCUST_VISIT_EFFECT"/>
	<ec:column width="50" property="PROCUST_VISIT_TYPE" title="拜访类型" mappingItem="PROCUST_VISIT_TYPE"/>
	<ec:column width="50" property="CUST_VISIT_CATEGORY" title="拜访类别" mappingItem="CUST_VISIT_CATEGORY"/>
</ec:row>
</ec:table>
<input type="hidden" name="PROCUST_VISIT_ID" id="PROCUST_VISIT_ID" value="" />
<input type="hidden" name="CUST_VISIT_CATEGORY" id="CUST_VISIT_CATEGORY" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" id="ORG_ID" name="ORG_ID" value="<%=pageBean.inputValue("ORG_ID")%>" />
<script language="JavaScript">
setRsIdTag('PROCUST_VISIT_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
