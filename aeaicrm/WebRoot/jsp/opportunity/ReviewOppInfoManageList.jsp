<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>商机管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function controlUpdateBtn(stateResult){
	if(stateResult =='0'){
		enableButton("editImgBtn");
		disableButton("createOrderImgBtn");
		enableButton("detailImgBtn");
		disableButton("confirmImgBtn");
		enableButton("disposeImgBtn");
		enableButton("delImgBtn");
	}
	if(stateResult =='1'){
		disableButton("editImgBtn");
		disableButton("createOrderImgBtn");
		enableButton("detailImgBtn");
		enableButton("confirmImgBtn");
		enableButton("disposeImgBtn");
		disableButton("delImgBtn");
	}
	if(stateResult =='2'){
		disableButton("editImgBtn");
		enableButton("createOrderImgBtn");
		enableButton("detailImgBtn");
		disableButton("confirmImgBtn");
		enableButton("disposeImgBtn");
		disableButton("delImgBtn");
	}
	if(stateResult =='3'){
		disableButton("editImgBtn");
		disableButton("createOrderImgBtn");
		disableButton("detailImgBtn");
		disableButton("confirmImgBtn");
		disableButton("disposeImgBtn");
		disableButton("delImgBtn");
	}
}
function goToBackList(){
	parent.closeBox();
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input id="detailImgBtn" value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>  
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBackList();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;状态<select id="OPP_STATE" label="状态" name="OPP_STATE" class="select" onchange="doQuery()"><%=pageBean.selectValue("OPP_STATE")%></select>
&nbsp;关注产品<input id="OPP_CONCERN_PRODUCT" label="关注产品" name="OPP_CONCERN_PRODUCT" type="text" value="<%=pageBean.inputValue("OPP_CONCERN_PRODUCT")%>" size="12" class="text"  ondblclick="emptyText('OPP_CONCERN_PRODUCT')" />
&nbsp;商机名称<input id="OPP_NAME" label="商机名称" name="OPP_NAME" type="text" value="<%=pageBean.inputValue("OPP_NAME")%>" size="12" class="text" ondblclick="emptyText('OPP_NAME')" />
&nbsp;跟进人<input id="CLUE_SALESMAN_NAME" label="跟进人" name="CLUE_SALESMAN_NAME" type="text" value="<%=pageBean.inputValue("CLUE_SALESMAN_NAME")%>" size="12" class="text" ondblclick="emptyText('CLUE_SALESMAN_NAME')" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" 
retrieveRowsCallback="process" 
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="${ec_rd == null ?15:ec_rd}"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{OPP_ID:'${row.OPP_ID}'});controlUpdateBtn('${row.OPP_STATE}');refreshConextmenu()" onclick="selectRow(this,{OPP_ID:'${row.OPP_ID}'});controlUpdateBtn('${row.OPP_STATE}');">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="OPP_NAME" title="商机名称"   />
	<ec:column width="100" property="CUST_ID_NAME" title="客户名称"   />
	<ec:column width="100" property="CONT_ID_NAME" title="联系人名称"   />
	<ec:column width="100" property="OPP_CONCERN_PRODUCT" title="关注产品" />
	<ec:column width="100" property="OPP_STATE" title="状态" mappingItem="OPP_STATE"/>
	<ec:column width="100" property="CLUE_SALESMAN_NAME" title="跟进人员"   />
	<ec:column width="100" property="OPP_CREATER_NAME" title="创建人"   />
	<ec:column width="100" property="OPP_CREATE_TIME" title="创建时间"   />
</ec:row>
</ec:table>
<input type="hidden" name="OPP_ID" id="OPP_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="custId" id="custId" value="<%=pageBean.inputValue("custId")%>" />
<script language="JavaScript">
setRsIdTag('OPP_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
